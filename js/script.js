$(document).ready(function () {

    $(".js-toggle-fancybox, .js-click-children-fancybox").fancybox({
        height: 'auto',
        fitToView: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            overlay: {
                locked: false
            }
        },
        padding: 0
    });

    // $('.form__name-block').on("click", ".js-click-children-fancybox", function(){
    //     $(this).children(".js-toggle-fancybox").trigger("click");
    // });

    $('.footer__rules, .footer__faq, .form__rules').on('click', function () {
        var targetModal = $(this).attr('href'),
            $targetScroll = $(targetModal).find('.js-scroll'),
            $targetScrollWrap = targetModal + '.js-scroll';

        // console.log($targetScrollWrap);

        $.fancybox.open([$(this)], {
            closeClick: false,
            //            maxHeight: 525,
            height: 'auto',
            fitToView: true,
            autoSize: false,
            openEffect: 'none',
            closeEffect: 'none',
            onUpdate: function () {
                $($targetScroll).getNiceScroll().resize();
            },
            beforeShow: function () {
                $($targetScroll).niceScroll({
                    cursorcolor: "#3991db",
                    cursoropacitymin: 1,
                    cursorwidth: "5px",
                    cursorminheight: 60,
                    cursorfixedheight: false,
                    cursorborder: "1px solid #3991db",
                    mousescrollstep: 5,
                    disablemutationobserver: true,
                    railpadding: {
                        right: 7
                    },
                });
                //$('.footer__rules').fancybox();


            },
            beforeClose: function () {
                $($targetScroll).getNiceScroll().remove();
            },
            padding: 0
        });

        return false;

    });

    var clipboard = new Clipboard('.user-info__button');
    
    clipboard.on('success', function(e) {
    console.info('Action:', e.action);
    console.info('Text:', e.text);
    console.info('Trigger:', e.trigger);

    e.clearSelection();
});

clipboard.on('error', function(e) {
    console.error('Action:', e.action);
    console.error('Trigger:', e.trigger);
});

    $(".fancybox").fancybox();

    $('.fr-scroll, .msg-scroll').slimScroll({
        position: 'right',
        height: '930px',
        railVisible: true,
        alwaysVisible: true,
        color: '#1095fd',
        railColor: '#e1f2ff',
        railOpacity: 0.5,
        allowPageScroll: true,
        wheelStep: 5,
        size: '10px'
    });
    
    $('.fr-msg-scroll').slimScroll({
        position: 'right',
        height: '1073px',
        railVisible: true,
        alwaysVisible: true,
        color: '#1095fd',
        railColor: '#e1f2ff',
        railOpacity: 0.5,
        allowPageScroll: true,
        wheelStep: 5,
        size: '10px'
    });
    
    $('.msg-scroll').slimScroll({
        position: 'right',
        height: '875px',
        railVisible: true,
        alwaysVisible: true,
        color: '#1095fd',
        railColor: '#e1f2ff',
        railOpacity: 0.5,
        allowPageScroll: true,
        wheelStep: 5,
        size: '10px'
    });

    $('.faq-scroll').slimScroll({
        position: 'right',
        height: '486px',
        railVisible: true,
        alwaysVisible: true,
        color: '#3991db',
        railColor: '#e1f2ff',
        railOpacity: 0.5,
        allowPageScroll: true,
        wheelStep: 5,
        size: '5px',

    });

    $('.rules-scroll').slimScroll({
        position: 'right',
        height: '424px',
        railVisible: true,
        alwaysVisible: true,
        color: '#3991db',
        railColor: '#e1f2ff',
        railOpacity: 0.5,
        allowPageScroll: true,
        wheelStep: 5,
        size: '5px'
    });
    
    $('.send-scroll').slimScroll({
        position: 'right',
        height: '120px',
        railVisible: true,
        alwaysVisible: true,
        color: '#3991db',
        railColor: '#e1f2ff',
        railOpacity: 0.5,
        allowPageScroll: true,
        wheelStep: 5,
        size: '5px'
    });

//    $("#emoji, #emoji2").emojioneArea({

    $('.emojionearea-editor').slimScroll({
        position: 'right',
        height: '120px',
        railVisible: true,
        alwaysVisible: true,
        color: '#3991db',
        railColor: '#e1f2ff',
        railOpacity: 0.5,
        allowPageScroll: true,
        wheelStep: 5,
        size: '5px'
    });
    $("#emoji").emojioneArea({
        events: {
            focus: function (editor, event) {
                $('.emojionearea-editor').slimScroll({
                    position: 'right',
                    height: '120px',
                    railVisible: true,
                    alwaysVisible: true,
                    color: '#3991db',
                    railColor: '#e1f2ff',
                    railOpacity: 0.5,
                    allowPageScroll: true,
                    wheelStep: 5,
                    size: '5px'
                });
            }
        },
        tonesStyle: "bullet"
    });
    //  });



    $(".js-enter").fancybox({
        height: 'auto',
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });

    $(".send-sms, js-confirm-phone").fancybox({
        maxWidth: 600,
        height: '100%',
        width: '100%',
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });

    $(".video-open").fancybox({
        maxWidth: 500,
        height: 500,
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });

    $(".video-open").fancybox({

        afterShow: function () {
            this.content.find('video').trigger('play')

            // Attach the ended callback to trigger the fancybox.next() once the video has ended.
            //      this.content.find('video').on('ended', function() {
            //        $.fancybox.next();
            //      });
        }
    });

    $(".password-check").fancybox({
        maxWidth: 583,
        maxHeight: 243,
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });
    
    $(".phone-change-1").fancybox({
        maxWidth: 583,
        width: '100%',
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });
    
    $(".phone-change-2").fancybox({
        maxWidth: 583,
        width: '100%',
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });
    
    $(".password-change").fancybox({
        maxWidth: 583,
        width: '100%',
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });
    
    $(".password-change-result").fancybox({
        maxWidth: 583,
        width: '100%',
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });
    
    $(".e-mail-change").fancybox({
        maxWidth: 583,
        width: '100%',
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });
    
    $(".e-mail-change-result").fancybox({
        maxWidth: 583,
        width: '100%',
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });
    
    $(".acc-del").fancybox({
        maxWidth: 583,
        width: '100%',
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });
    
    $(".phone-change-result").fancybox({
        maxWidth: 583,
        width: '100%',
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });
    
//    jQuery(document).ready(function(){
//    jQuery('.textarea-scrollbar').scrollbar();
//});
        
    //countdown timer
    var clock;

			clock = $('.your-clock').FlipClock({
		        clockFace: 'DailyCounter',
		        autoStart: false,
                language: 'ru-ru',
		        callbacks: {
		        	stop: function() {
		        		$('.message').html('The clock has stopped!')
		        	}
		        }
		    });
    
    
				    
		    clock.setTime(220880);
		    clock.setCountdown(true);
		    clock.start();
    
    


    $('.send-request').on('click', function (e) {

        var target = '#' + $(this).data('href'),
            areas = $('.js-form').find('input');

        areas.each(function () {
            var el = $(this);

            if (el.is(':invalid')) {
                console.log('error')
                return false;
            } else {
                $.fancybox.open([$(target)], {
                    maxWidth: 960,
                    maxHeight: 240,
                    autoHeight: true,
                    width: '100%',
                    height: '100%',
                    fitToView: true,
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none',
                    padding: 0
                });
            }
        });


        e.preventDefault();
    });

    $(".password-check-back").fancybox({
        fitToView: true,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });


    $(".footer__connect").fancybox({
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0
    });

    $('.close').click(function (event) {
        event.preventDefault();
        $('.send-message__uploaded').empty();
        $('.close').empty();
        //    $('.send-message__uploaded').css('display', 'none');
        //    $('.close').css('display', 'none');
    });

    $(".b-hamburger").click(function (event) {
        event.preventDefault();
        $("span").toggleClass("active");
        $(".navigation").toggleClass("nav-show");
    });
    $.validator.addMethod("lettersonly", function (value, element) {
        var regex = /^[a-zA-Z]+$/g;
        var rusregex = /^[а-яА-Я]+$/g;
        if (regex.test(value)) {
            return this.optional(element) || /^[a-zA-Z]/.test(value);
        } else if (rusregex.test(value)) {
            return this.optional(element) || /^[а-яА-Я]+$/.test(value);
        }
    }, 'Неккоректно введено имя');

    $.validator.addMethod("lettersonlysurname", function (value, element) {
        var regex = /^[a-zA-Z]+$/g;
        var rusregex = /^[а-яА-Я]+$/g;
        if (regex.test(value)) {
            return this.optional(element) || /^[a-zA-Z]/.test(value);
        } else if (rusregex.test(value)) {
            return this.optional(element) || /^[а-яА-Я]+$/.test(value);
        }
    }, 'Неккоректно введена фамилия');

    $.validator.addMethod("password", function (value, element) {
        var regex = /^[a-zA-Z0-9]+$/g;
        return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
    }, 'Неккоректно введен пароль');

    $.validator.addMethod("onlyemail", function (value, element) {
        return this.optional(element) || /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(value);
    }, 'Неккоректно введен e-mail');

    $.validator.addMethod("onlyphone", function (value, element) {
        return this.optional(element) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value);
    }, 'Неккоректно введен телефон');

    $.validator.methods.email = function (value, element) {
        if (value.indexOf("@") != -1) {
            return this.optional(element) || /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(value);
        } else {
            return this.optional(element) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value);
        }
    }
    $.validator.setDefaults({
        submitHandler: function () {
            alert("submitted!");
        }
    });
    $("#registerform").validate({
        wrapper: "em",
        highlight: function (element, errorClass, validClass) {
            $(element).parent().addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent().removeClass(errorClass).addClass(validClass);
        },
        success: function (label) {
            label.addClass("valid").text("| Всё правильно");
            label.parent().parent().addClass("valid");
        },
        rules: {
            username: {
                required: true,
                lettersonly: true,
                minlength: 3,
                maxlength: 15
            },
            surname: {
                required: true,
                lettersonlysurname: true,
                minlength: 3,
                maxlength: 15
            },
            userid: {
                required: true,
                number: true
            },
            password: {
                required: true,
                password: true,
                minlength: 5,
                maxlength: 32
            },
            email: {
                minlength: 6,
                email: true,
                required: true
            }
        },
        messages: {
            username: {
                required: "Введите Ваше имя",
                minlength: "Поле должно содержать минимум 4 символа",
                maxlength: "Поле не должно содержать более 15 символов"
            },
            surname: {
                required: "Введите Вашу фамилию",
                minlength: "Поле должно содержать минимум 4 символа"
            },
            userid: {
                required: "Введите ID пользователя",
                number: "ID может содержать только цыфры"
            },
            password: {
                required: "Введите пароль",
                minlength: "Пароль должен содержать минимум 5 символов",
                maxlength: "Пароль должен быть не больше 32 символов"
            },
            email: {
                minlength: "Заполните поле правильно",
                email: "Неккоректно введен e-mail или телефон",
                required: "Введите e-mail или телефон",
            }
        }
    });
    $("#contactform").validate({
        wrapper: "em",
        highlight: function (element, errorClass, validClass) {
            $(element).parent().addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent().removeClass(errorClass).addClass(validClass);
        },
        success: function (label) {
            label.addClass("valid").text("| Всё правильно");
            label.parent().parent().addClass("valid");
        },
        rules: {
            connectname: {
                required: true,
                minlength: 3,
                maxlength: 15
            },
            connectmail: {
                required: true,
                email: true
            }
        },
        messages: {
            connectname: {
                required: "Введите Ваше имя",
                minlength: "Поле должно содержать минимум 4 символа"
            },
            connectmail: {
                email: "Неккоректно введен e-mail",
                required: "Поле не может быть пустым",
            }
        }
    });

    // Validate modal forms @ settings page
    
    // Change E-mail 1 screen
    $("#change-email-modal-form").validate({
        wrapper: "em",
        highlight: function (element, errorClass, validClass) {
            $(element).parent().addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent().removeClass(errorClass).addClass(validClass);
        },
        success: function (label) {
            label.addClass("valid").text("| Всё правильно");
            label.parent().parent().addClass("valid");
        },
        rules: {
            emailchange2: {
                onlyemail: true,
                required: true
            }
        },
        messages: {
            emailchange2: {
                onlyemail: "Неккоректно введен e-mail или телефон",
                required: "Введите e-mail"
            }
        }
    });
    
    // Change Phone 1 screen
    $("#change-phone-modal-form").validate({
        wrapper: "em",
        highlight: function (element, errorClass, validClass) {
            $(element).parent().addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent().removeClass(errorClass).addClass(validClass);
        },
        success: function (label) {
            label.addClass("valid").text("| Всё правильно");
            label.parent().parent().addClass("valid");
        },
        rules: {
            telchange: {
                minlength: 6,
                onlyphone: true,
                required: true
            }
        },
        messages: {
            telchange: {
                minlength: "Заполните поле правильно",
                onlyphone: "Неккоректно введен телефон",
                required: "Введите телефон"
            }
        }
    });
    
    // Change Password
    $("#change-password-modal-form").validate({
        wrapper: "em",
        highlight: function (element, errorClass, validClass) {
            $(element).parent().addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent().removeClass(errorClass).addClass(validClass);
        },
        success: function (label) {
            label.addClass("valid").text("| Всё правильно");
            label.parent().parent().addClass("valid");
        },
        rules: {
            oldpasschange: {
                required: true
            },
            passchange2: {
                required: true,
                password: true,
                minlength: 5,
                maxlength: 32
            },
            passchange3: {
                equalTo: "#passchange2"
            }
        },
        messages: {
            oldpasschange: {
                required: "Введите старый пароль"
            },
            passchange2: {
                required: "Введите новый пароль",
                minlength: "Пароль должен содержать минимум 5 символов",
                maxlength: "Пароль должен быть не больше 32 символов"
            },
            passchange3: {
                equalTo: "Пароли должны совпадать"
            }
        }
    });

    //old
    $('#upload').on('change', function () {
        var $parent = $('.send-message__up-files');
        var fileList = document.getElementById('upload').files,
            maxElement = 3;

//        $('.send-message__up-files-block').remove();
        for (var i = 0; i < maxElement; i++) {
            var item = fileList[i];

            if (item != null) {
                var name = item.name;
                var fileItem = '<div class="send-message__up-files-block">' +
                    '<span class="send-message__uploaded">' + name +
                    '</span><span class="close" data-index="' + i + '"></span></div>';

                $parent.prepend(fileItem);
            }
        };
    });



    $('.send-message__up-files').on('click', '.close', function (e) {
        var fileList = document.getElementById('upload').files;
        var index = $(this).data('index');

        $(this).closest('.send-message__up-files-block').remove();

        //fileList[index] = null;
        //delete fileList[index];

    });
    //    
    $('select').selectric();
    
    $('#upload-test').multifile();

    $('.js-file').on('change', function () {
        var realVal = $(this).val(),
            lastIndex = realVal.lastIndexOf('\\') + 1;
        var $parent = $('.form__avatar-containerWrap');
        
        $('.form__avatar-container').remove();
        if (lastIndex !== -1) {
            realVal = realVal.substr(lastIndex);
            
             var fileItem = '<div class="form__avatar-container"><span class="form__avatar-result js-fileName">' +        realVal + '</span><span class="js-close"></span></div>'

            console.log(realVal);
             $parent.prepend(fileItem);
        };
    });

    $('.js-fileWrap').on('click', '.js-close', function (e) {

        $(this).closest('.form__avatar-container').remove();

    });


});